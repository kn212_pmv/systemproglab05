﻿// Calculation.h
#pragma once

class ICalculation {
public:
    virtual int PerformCalculation(int a, int b) = 0;
};

// Оголошення функції експорту для отримання екземпляра класу обчислення
extern "C" __declspec(dllexport) ICalculation * GetCalculationInstance();
