﻿// Calculation.cpp
#include "Calculation.h"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <algorithm>
#include <thread>

class Calculation : public ICalculation {
public:
    int PerformCalculation(int a, int b) override {
        // Підключення не власної бібліотеки та використання її функцій для прикладу
        std::cout << "Performing external calculation using std::rand(): " << std::rand() << std::endl;

        // Алгоритм повного перебору
        std::vector<int> numbers;
        for (int i = 1; i <= 5; ++i) {
            numbers.push_back(i);
        }

        std::cout << "Numbers before permutation: ";
        for (int num : numbers) {
            std::cout << num << " ";
        }
        std::cout << std::endl;

        std::random_shuffle(numbers.begin(), numbers.end());

        std::cout << "Numbers after permutation: ";
        for (int num : numbers) {
            std::cout << num << " ";
        }
        std::cout << std::endl;

        // Породження багатьох потоків
        std::vector<std::thread> threads;
        for (int i = 0; i < 5; ++i) {
            threads.emplace_back(&Calculation::ThreadFunction, this, i + 1);
        }

        for (auto& thread : threads) {
            thread.join();
        }

        return a * b; // Приклад різного обчислення
    }

    // Функція, яку використовують потоки
    void ThreadFunction(int id) {
        std::cout << "Thread " << id << " is running." << std::endl;
        // Додаткові обчислення для потоків
    }
};

// Реалізація функції експорту
extern "C" __declspec(dllexport) ICalculation * GetCalculationInstance() {
    return new Calculation();
}
