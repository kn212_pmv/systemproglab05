﻿// Interface.cpp
#include "Interface.h"

class Interface : public IInterface {
public:
    int Calculate(int a, int b) override {
        return a + b;
    }
};

// Реалізація функції експорту
extern "C" __declspec(dllexport) IInterface * GetInterfaceInstance() {
    return new Interface();
}
