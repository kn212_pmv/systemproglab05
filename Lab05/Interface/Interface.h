﻿// Interface.h
#pragma once

class IInterface {
public:
    virtual int Calculate(int a, int b) = 0;
};

// Оголошення функції експорту для отримання екземпляра класу інтерфейсу
extern "C" __declspec(dllexport) IInterface * GetInterfaceInstance();
