﻿// Interface.h
#pragma once

class IInterface {
public:
    virtual int Calculate(int a, int b) = 0;
};
