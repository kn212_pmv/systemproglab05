﻿// Calculation.cpp
#include "Interface.h"

class Calculation : public IInterface {
public:
    int Calculate(int a, int b) override {
        return a + b;
    }
};

class AdvancedCalculation : public IInterface {
public:
    int Calculate(int a, int b) override {
        return a * b; // Як приклад різного обчислення
    }
};

extern "C" __declspec(dllexport) IInterface * GetInterfaceInstance(bool useAdvanced) {
    if (useAdvanced) {
        return new AdvancedCalculation();
    }
    else {
        return new Calculation();
    }
}
