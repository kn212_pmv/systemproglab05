﻿// TestProject.cpp
#include <iostream>
#include <windows.h>
#include "C:\Users\pavlo\source\repos\sysproglab05\Project1\Interface\Interface.h""
#include "C:\Users\pavlo\source\repos\sysproglab05\Project1\Calculation\Calculation.h"
// Оголошення функцій для завантаження та вивантаження DLL
IInterface* LoadInterfaceDLL();
void UnloadInterfaceDLL(IInterface* interfaceInstance);

ICalculation* LoadCalculationDLL();
void UnloadCalculationDLL(ICalculation* calculationInstance);

int main() {
    // Завантаження та вивантаження DLL з інтерфейсом
    IInterface* interfaceInstance = LoadInterfaceDLL();

    if (interfaceInstance) {
        int resultInterface = interfaceInstance->Calculate(5, 3);
        std::cout << "Result from Interface: " << resultInterface << std::endl;

        // Вивантаження DLL з інтерфейсом
        UnloadInterfaceDLL(interfaceInstance);
    }
    else {
        std::cerr << "Failed to load Interface DLL." << std::endl;
        return 1;
    }

    // Завантаження та вивантаження DLL для обчислень (пізніше, за вимогою користувача)
    std::cout << "Press Enter to load Calculation DLL." << std::endl;
    std::cin.get();

    ICalculation* calculationInstance = LoadCalculationDLL();

    if (calculationInstance) {
        int resultCalculation = calculationInstance->PerformCalculation(5, 3);
        std::cout << "Result from Calculation: " << resultCalculation << std::endl;

        // Вивантаження DLL для обчислень
        UnloadCalculationDLL(calculationInstance);
    }
    else {
        std::cerr << "Failed to load Calculation DLL." << std::endl;
        return 1;
    }

    return 0;
}


// Допоміжні функції для завантаження та вивантаження DLL з інтерфейсом
IInterface* LoadInterfaceDLL() {
    HINSTANCE interfaceDll = LoadLibrary(L"Interface.dll");

    if (interfaceDll) {
        auto getInterface = reinterpret_cast<IInterface * (*)()>(GetProcAddress(interfaceDll, "GetInterfaceInstance"));
        if (getInterface) {
            return getInterface();
        }
    }

    return nullptr;
}

void UnloadInterfaceDLL(IInterface* interfaceInstance) {
    delete interfaceInstance;
    // Вивантаження DLL з інтерфейсом
}

// Допоміжні функції для завантаження та вивантаження DLL для обчислень
ICalculation* LoadCalculationDLL() {
    HINSTANCE calculationDll = LoadLibrary(L"Calculation.dll");

    if (calculationDll) {
        auto getCalculation = reinterpret_cast<ICalculation * (*)()>(GetProcAddress(calculationDll, "GetCalculationInstance"));
        if (getCalculation) {
            return getCalculation();
        }
    }

    return nullptr;
}

void UnloadCalculationDLL(ICalculation* calculationInstance) {
    delete calculationInstance;
    // Вивантаження DLL для обчислень
}